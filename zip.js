var AdmZip = require("adm-zip");
var zip = new AdmZip();

zip.addLocalFolder('ex1/src', 'ex1', /^((?!(node_modules)).)*$/);
zip.addLocalFolder('ex2/src', 'ex2',  /^((?!(node_modules)).)*$/);
zip.addLocalFile('README.md');
zip.writeZip('interrogation.zip');
